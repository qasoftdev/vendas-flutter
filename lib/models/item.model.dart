import 'dart:convert';

import 'order_item_pk.model.dart';
import 'product.model.dart';

class Item {
  OderItemPK? oderItemPK;
  int? qtdade;
  Product? product;

  Item(
    this.oderItemPK,
    this.qtdade,
    this.product,
  );

  Item.create(this.qtdade, this.product);

  Map<String, dynamic> newItemToMap() {
    return {"qtdade": qtdade, "product": product};
  }

  Map<String, dynamic> fullItemToMap() {
    return {"oderItemPK": oderItemPK, "qtdade": qtdade, "product": product};
  }

  static Item fromMap(Map<String, dynamic> map) {
    return Item(
      map["oderItemPK"],
      map["qtdade"],
      map["product"],
    );
  }

  static List<Item> fromMaps(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Item.fromMap(maps[i]);
    });
  }

  static Item fromJson(String json) => Item.fromMap(jsonDecode(json));

  static List<Item> fromJsonList(String json) {
    final parsed = jsonDecode(json).cast<Map<String, dynamic>>();
    return parsed.map<Item>((map) => Item.fromMap(map)).toList();
  }

  String newItemToJson() => jsonEncode(newItemToMap());

  String fullClienteToJson() => jsonEncode(fullItemToMap());
}
