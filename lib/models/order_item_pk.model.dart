import 'order.model.dart';
import 'product.model.dart';

class OderItemPK {
  Order order;
  Product product;

  OderItemPK({
    required this.order,
    required this.product,
  });
}
