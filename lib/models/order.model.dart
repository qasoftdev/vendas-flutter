import 'dart:convert';
import 'client.model.dart';
import 'item.model.dart';

class Order {
  int? id;
  String? date;
  Client? client;
  List<Item>? items;

  Order(
    this.id,
    this.date,
    this.items,
    this.client,
  );

  Order.create(String text, {this.date, this.items, this.client});

  Map<String, dynamic> newOrderToMap() {
    return {"date": date, "items": items, "client": client};
  }

  Map<String, dynamic> fullOrderToMap() {
    return {"id": id, "date": date, "items": items, "client": client};
  }

  static Order fromMap(Map<String, dynamic> map) {
    return Order(map["id"], map["date"], map["items"], map["client"]);
  }

  static List<Order> fromMaps(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Order.fromMap(maps[i]);
    });
  }

  static Order fromJson(String json) => Order.fromMap(jsonDecode(json));

  static List<Order> fromJsonList(String json) {
    final parsed = jsonDecode(json).cast<Map<String, dynamic>>();
    return parsed.map<Order>((map) => Order.fromMap(map)).toList();
  }

  String newOrderToJson() => jsonEncode(newOrderToMap());

  String fullClienteToJson() => jsonEncode(fullOrderToMap());
}
