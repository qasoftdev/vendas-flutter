import 'package:vendas_flutter/view/update_client_page.dart';
import 'package:vendas_flutter/view/update_order.dart';

import '../view/list_clients_page.dart';
import '../view/list_order_page.dart';
import '../view/list_product_page.dart';
import '../view/new_client_page.dart';
import '../view/new_order_page.dart';
import '../view/new_product_page.dart';
import '../view/update_product.dart';

class Routes {
  static const String listProducts = ListProductPage.routeName;
  static const String newProduct = NewProductPage.routeName;
  static const String updateProduct = UpdateProductPage.routeName;
  static const String listClients = ListClientPage.routeName;
  static const String newClient = NewClientPage.routeName;
  static const String updateClient = UpdateClientPage.routeName;
  static const String listOrders = ListOrderPage.routeName;
  static const String newOrder = NewOrderPage.routeName;
  static const String updateOrder = UpdateOrderPage.routeName;
}
