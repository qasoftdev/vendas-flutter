import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:vendas_flutter/rest/api.dart';
import '../ErrorDTO/error.dto.dart';
import '../models/order.model.dart';
import '../models/product.model.dart';

class OrderRest {
  Future<Order> findById(int id) async {
    final http.Response response =
        await http.get(Uri.http(API.endpoint, '/orders/${id}'));
    if (response.statusCode == 200) {
      return Order.fromJson(response.body);
    } else {
      throw Exception(
          'Erro ao buscar pedido: $id [code: ${response.statusCode}]');
    }
  }

  Future<List<Order>> findAll() async {
    final http.Response response =
        await http.get(Uri.http(API.endpoint, 'orders'));
    if (response.statusCode == 200) {
      return Order.fromJsonList(response.body);
    } else {
      throw Exception(
          'Erro ao buscar todos os pedidos.Motivo: ${ErrorDTO.fromJson(response.body).message}');
    }
  }

  Future<Order> save(Order order) async {
    final http.Response response =
        await http.post(Uri.http(API.endpoint, 'orders'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: order.newOrderToJson());
    if (response.statusCode == 201) {
      return Order.fromJson(response.body);
    } else {
      throw Exception(
          'Erro ao inserir Pedido. Motivo: ${ErrorDTO.fromJson(response.body).message}');
    }
  }

  Future<Order> update(Order order) async {
    final http.Response response =
        await http.put(Uri.http(API.endpoint, 'orders/${order.id}'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: order.fullClienteToJson());
    if (response.statusCode == 200) {
      return order;
    } else {
      throw Exception(
          'Erro ao alterar o pedido: ${order.id} .Motivo: ${ErrorDTO.fromJson(response.body).message}');
    }
  }

  Future<Order> delete(Order order) async {
    final http.Response response = await http.delete(
        Uri.http(API.endpoint, 'orders/${order.id}'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });
    if (response.statusCode == 200) {
      return Order.fromJson(json.decode(response.body));
    } else {
      throw Exception(
          'Erro ao remover o pedido: ${order.id} .Motivo: ${ErrorDTO.fromJson(response.body).message}');
    }
  }
}
