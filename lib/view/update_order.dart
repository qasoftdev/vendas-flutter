import 'package:flutter/material.dart';
import 'package:vendas_flutter/models/order.model.dart';
import 'package:vendas_flutter/routes/routes.dart';
import 'package:vendas_flutter/utils/error_handler.dart';
import 'package:vendas_flutter/widgets/drawer.dart';

import '../repository/order.repository.dart';

class UpdateOrderPage extends StatefulWidget {
  static const String routeName = "/update-order";

  const UpdateOrderPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _UpdateOrderState();
}

class _UpdateOrderState extends State<UpdateOrderPage> {
  final _formKey = GlobalKey<FormState>();
  final _descriptionController = TextEditingController();

  OrderRepository repository = OrderRepository();

  int _id = 0;
  Order? _order;

  @override
  void dispose() {
    _descriptionController.dispose();
    super.dispose();
  }

  void _findOrder() async {
    try {
      _order = await repository.findById(_id);
      _descriptionController.text = _order!.date!;
    } catch (exception) {
      ErrorHandler()
          .showError(context, "Erro ao abrir produto", exception.toString());
    }
  }

  Future<Order?> _saveOrder() async {
    _order!.date = _descriptionController.text;
    try {
      await repository.update(_order!);
    } catch (exception) {
      ErrorHandler().showError(
          context, "Erro ao salvar a edição do produto", exception.toString());
    }

    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Produto editado com sucesso')));
  }

  Widget _buildForm(BuildContext context) {
    return Column(
      children: [
        Form(
            key: _formKey,
            child: ListView(shrinkWrap: true, children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text("Descrição: "),
                  Expanded(
                      child: TextFormField(
                    controller: _descriptionController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Campo não pode ser vazio";
                      }
                      return null;
                    },
                  ))
                ],
              ),
              Row(children: [
                ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        await _saveOrder();
                        Navigator.pushNamed(context, Routes.listOrders);
                      }
                    },
                    child: const Text("Salvar")),
                ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Cancelar'),
                ),
              ])
            ]))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final Map map = ModalRoute.of(context)!.settings.arguments as Map;
    _id = map["id"];
    _findOrder();

    return Scaffold(
        appBar: AppBar(
          title: const Text("Editar produto"),
        ),
        drawer: const AppDrawer(),
        body: _buildForm(context));
  }
}
