import 'package:flutter/material.dart';
import 'package:vendas_flutter/models/order.model.dart';
import 'package:vendas_flutter/repository/order.repository.dart';
import 'package:vendas_flutter/routes/routes.dart';
import 'package:vendas_flutter/utils/error_handler.dart';
import 'package:vendas_flutter/view/update_order.dart';
import 'package:vendas_flutter/widgets/drawer.dart';

class ListOrderPage extends StatefulWidget {
  const ListOrderPage({Key? key}) : super(key: key);
  static const String routeName = "/list-orders";

  @override
  State<StatefulWidget> createState() => _ListOrderPage();
}

class _ListOrderPage extends State<ListOrderPage> {
  List<Order> _orderList = [];
  OrderRepository repository = OrderRepository();

  @override
  void initState() {
    super.initState();
    _refreshList();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _refreshList() async {
    List<Order> tempList = await _findAll();
    setState(() {
      _orderList = tempList;
    });
  }

  Future<List<Order>> _findAll() async {
    List<Order> orderList = <Order>[];
    try {
      orderList = await repository.findAll();
    } catch (exception) {
      ErrorHandler()
          .showError(context, "Erro ao listar produtos", exception.toString());
    }
    return orderList;
  }

  Future<Order> _removeorder(Order order) async {
    try {
      await repository.remove(order);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Produto removido com suecsso')));
    } catch (exception) {
      ErrorHandler()
          .showError(context, "Erro ao remover produto", exception.toString());
    }
    return order;
  }

  void _showorder(BuildContext context, int index) {
    Order order = _orderList[index];
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text("Id => ${order.id}"),
              content: Column(
                children: [
                  Text("Id: ${order.id}"),
                  Text("Date: ${order.date}")
                ],
              ),
              actions: [
                TextButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: const Text("Fechar"))
              ]);
        });
  }

  void _updateProdut(BuildContext context, int index) {
    Order order = _orderList[index];

    Navigator.pushNamed(context, UpdateOrderPage.routeName,
        arguments: <String, int>{"id": order.id!});
  }

  void _removeItem(BuildContext context, int index) {
    Order order = _orderList[index];
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: const Text("Remover produto"),
              content: Text("Remover o produto ${order.id}?"),
              actions: [
                TextButton(
                    onPressed: () => {Navigator.of(context).pop()},
                    child: const Text("Não")),
                TextButton(
                    onPressed: () async {
                      await _removeorder(order);
                      _refreshList();
                      Navigator.of(context).pop();
                    },
                    child: const Text("Sim"))
              ],
            ));
  }

  ListTile _buildItem(BuildContext context, int index) {
    Order order = _orderList[index];

    return ListTile(
        leading: const Icon(Icons.new_label_outlined),
        title: Text("${order.id}"),
        onTap: () {
          _showorder(context, index);
        },
        trailing: PopupMenuButton(itemBuilder: (context) {
          return [
            const PopupMenuItem(value: "edit", child: Text("Editar")),
            const PopupMenuItem(value: "delete", child: Text("Remover")),
          ];
        }, onSelected: (String option) {
          option == "edit"
              ? _updateProdut(context, index)
              : _removeItem(context, index);
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Lista de produtos")),
        body: ListView.builder(
            itemCount: _orderList.length, itemBuilder: _buildItem),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, Routes.newOrder);
          },
          tooltip: "Adicionar Pedido",
          backgroundColor: Colors.green,
          child: const Icon(Icons.add),
        ));
  }
}
