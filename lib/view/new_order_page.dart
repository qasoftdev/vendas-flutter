import 'package:flutter/material.dart';
import 'package:vendas_flutter/models/order.model.dart';
import 'package:vendas_flutter/routes/routes.dart';
import 'package:vendas_flutter/utils/error_handler.dart';
import 'package:vendas_flutter/widgets/drawer.dart';

import '../repository/order.repository.dart';

class NewOrderPage extends StatefulWidget {
  const NewOrderPage({Key? key}) : super(key: key);
  static const String routeName = "/new-order";

  @override
  State<StatefulWidget> createState() => _NewOrderPageState();
}

class _NewOrderPageState extends State<NewOrderPage> {
  final _formKey = GlobalKey<FormState>();
  final _descriptionController = TextEditingController();

  OrderRepository repository = OrderRepository();

  @override
  void dispose() {
    _descriptionController.dispose();
    super.dispose();
  }

  Future<Order?> _saveOrder() async {
    Order? newOrder;
    try {
      newOrder =
          await repository.save(Order.create(_descriptionController.text));
    } catch (exception) {
      ErrorHandler()
          .showError(context, "Erro ao salvar produto", exception.toString());
    }

    _descriptionController.clear();

    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Produto salvo com sucesso")));

    return newOrder;
  }

  Widget _buildForm(BuildContext context) {
    return Column(
      children: [
        Form(
            key: _formKey,
            child: ListView(shrinkWrap: true, children: [
              Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                const Text("Descrição: "),
                Expanded(
                    child: TextFormField(
                  controller: _descriptionController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Campo não pode ser vazio";
                    }
                    return null;
                  },
                ))
              ]),
              Row(children: [
                ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        await _saveOrder();
                        Navigator.pushNamed(context, Routes.listOrders);
                      }
                    },
                    child: const Text("Salvar"))
              ])
            ]))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Cadastrar novo produto"),
        ),
        drawer: const AppDrawer(),
        body: _buildForm(context));
  }
}
