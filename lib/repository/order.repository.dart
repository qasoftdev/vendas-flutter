import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/order.model.dart';
import '../rest/api.dart';
import '../rest/order_rest.dart';

class OrderRepository {
  final OrderRest restInterface = OrderRest();

  Future<Order> findById(int id) async {
    return await restInterface.findById(id);
  }

  Future<List<Order>> findAll() async {
    return await restInterface.findAll();
  }

  Future<Order> save(Order order) async {
    return await restInterface.save(order);
  }

  Future<Order> update(Order order) async {
    return await restInterface.update(order);
  }

  Future<Order> remove(Order order) async {
    return await restInterface.delete(order);
  }
}
